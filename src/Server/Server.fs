module Server

open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open FSharp.Control.Tasks
open Giraffe
open Microsoft.AspNetCore.Authentication
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.AspNetCore.Authentication.OpenIdConnect
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Http
open Microsoft.Identity.Web
open Microsoft.Extensions.DependencyInjection
open Saturn
open System.IO

open Shared

type Storage() =
    let todos = ResizeArray<_>()

    member __.GetTodos() = List.ofSeq todos

    member __.AddTodo(todo: Todo) =
        if Todo.isValid todo.Description then
            todos.Add todo
            Ok()
        else
            Error "Invalid todo"

let storage = Storage()

storage.AddTodo(Todo.create "Create new SAFE project")
|> ignore

storage.AddTodo(Todo.create "Write your app")
|> ignore

storage.AddTodo(Todo.create "Ship it !!!")
|> ignore

let todosApi =
    { getTodos = fun () -> async { return storage.GetTodos() }
      addTodo =
          fun todo ->
              async {
                  match storage.AddTodo todo with
                  | Ok () -> return todo
                  | Error e -> return failwith e
              } }

let webApi : HttpHandler =
    Remoting.createApi ()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue todosApi
    |> Remoting.buildHttpHandler

let Unauthorized401 : HttpHandler =
    setStatusCode StatusCodes.Status401Unauthorized >=> text "401 Unauthorized"

let NotFound404 : HttpHandler =
    setStatusCode StatusCodes.Status404NotFound >=> text "404 Not Found"

let authenticate : HttpHandler =
    Auth.requiresAuthentication Unauthorized401

// Issue an OIDC authentication challenge if the user is not already authenticated.
// This will redirect to the Microsoft login page and allow the user to sign in to this app.
let signIn : HttpHandler =
    Auth.requiresAuthentication (Auth.challenge OpenIdConnectDefaults.AuthenticationScheme)

// Sign out from both authentication schemes.
// Not sure if both are necessary here.
let signOut : HttpHandler =
    fun (next: HttpFunc) (context: HttpContext) ->
        task {
            do! context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme)
            do! context.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme)
            return! next context
        }

let WebAppFiles = Directory.EnumerateFiles("public") |> Set.ofSeq

// Serve the web app files via a handler so they can be put behind an authentication barrier.
let webApp fileName =
    fun (next: HttpFunc) (context: HttpContext) ->
        task {
            let filePath = Path.Combine("public", fileName)
            // Check that the file path is for an actual web app file to prevent loading
            // files that are not meant to be seen. Without this, it's possible to load
            // files in parent directories using relative paths, which is not ideal!
            if WebAppFiles.Contains filePath
            then return! streamFile false filePath None None next context
            else return! NotFound404 next context
        }

let router =
    choose [
        // The only unauthenticated route is the sign-in handler, which redirects to
        // the web app when complete.
        route "/" >=> signIn >=> redirectTo false "/webApp/index.html"
        // All other routes sit behind an authentication barrier.
        authenticate
        >=> choose [
            routeCif "/webApp/%s" webApp
            webApi
            routeCi "/signOut" >=> signOut
            NotFound404 ] ]

let configureServices (services: IServiceCollection) =
    services
        .AddCors()
        .AddAuthentication(defaultScheme = OpenIdConnectDefaults.AuthenticationScheme)
        .AddMicrosoftIdentityWebApp(
            (fun options ->
                // Configure Microsoft Identity to authenticate against a particular
                // app registration in Azure AD (specified by the ClientId).
                options.Instance <- "https://login.microsoftonline.com/"
                options.TenantId <- "perpetuum.com"
                options.ClientId <- "3de4bbfb-28a6-4208-866e-f4bfb94ca7a9"),
            openIdConnectScheme = OpenIdConnectDefaults.AuthenticationScheme,
            cookieScheme = CookieAuthenticationDefaults.AuthenticationScheme)
        .Services

let configureApp (appBuilder: IApplicationBuilder) =
    appBuilder
        .UseCors(fun policyBuilder ->
            policyBuilder
                // Allow cross-origin requests to the login domain.
                .WithOrigins("https://login.microsoftonline.com")
                .AllowAnyMethod()
                .AllowAnyHeader()
            |> ignore)
        .UseAuthentication()

let app =
    application {
        url "http://0.0.0.0:8085"
        service_config configureServices
        app_config configureApp
        use_router router
        memory_cache
        // Don't serve the web app as static content so we can ensure access is authenticated.
        //use_static "public"
        use_gzip
    }

run app
